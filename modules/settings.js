/*
 * ImaVi settings module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

app.settings = {
	//Secondary variables
		formElements: undefined,
		
		list: {},
		
		defaultList: {
			smoothImages: true,
			autoZoomImages: true
		},
	
	//Methods
		apply: function() {
			for (var i = 0; i < this.formElements.length; i++) {
				this.list[this.formElements[i].name] = JSON.parse(this.formElements[i].value);
			}
			
			this.update();
			this.config.write();
		},
		
		update: function() {
			for (var i = 0; i < this.formElements.length; i++) {
				this.formElements[i].value = this.list[this.formElements[i].name];
			}
		},
	
	//Config manager
		config: {
			//Secondary variables
				path: "config.json",
			
			//Methods
				read: function() {
					Object.assign(app.settings.list, app.settings.defaultList);
					
					var config = "";
					try {
						config = fs.readFileSync(this.path, "utf-8");
					} catch(e) {}
					
					if (config) {
						try {
							app.settings.list = JSON.parse(config);
						} catch(e) {
							alert(e);
						}
					}
				},
				
				write: function(mode, parameter) {
					try {
						fs.writeFileSync(this.path, JSON.stringify(app.settings.list));
					} catch(e) {
						alert(e);
					}
				}
		}
};
