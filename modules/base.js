/*
 * ImaVi base module.
 * (C) 2016 Lupshenko.
 * This code is licensed under the GPL3.
 */

//Variables
	var 
		//Secondary variables
			fs = require("fs"),
			gui = require("nw.gui"),
			win = require("nw.gui").Window.get(),
		
		//Main block
			app = {
				//Secondary variables
					uiMask: undefined,
				
				//[run, init] methods and objects
					run: function(argv) {
						this.init();
						
						//Read arguments
							if (argv.length > 0) {
								var
									images = [],
									folders = [];
								
								//Get images	
									for (var i = 0; i < argv.length; i++) {
										var 
											folder = "",
											file = "";
										
										if (argv[i].indexOf("/") + 1) {
											folder = argv[i].slice(0, argv[i].lastIndexOf("/") + 1);
											file = argv[i].slice(argv[i].lastIndexOf("/") + 1, argv[i].length);
											
											images.push([folder, file]);
										} else {
											folder = "./";
											file = argv[i];
											
											images.push([folder, file]);
										}
									}
								
								//Get folders
									for (var i = 0; i < images.length; i++) {
										var
											folder = images[i][0], 
											file = images[i][1],
											
											folderContent = [],
											fileIndex = 0;
										
										//Get folder content
											try {
												folderContent = fs.readdirSync(folder).filter(function(element) {
													for (var j = 0; j < app.workArea.imageExtensions.length; j++) {
														if (element.toLowerCase().indexOf(
															app.workArea.imageExtensions[j]
														) + 1) {
															return true;
														}
													}
													
													return false;
												});
											} catch(e) {
												alert(new ReferenceError(
													"folder \"" + folder + "\" doesn't exist"
												));
												
												continue;
											}
										
										//Get file index
											fileIndex = folderContent.indexOf(file);
											
											if (!(fileIndex + 1)) {
												alert(new ReferenceError(
													"file \"" + folder + file + "\" isn't image " +
													"or doesn't exist"
												));
												
												continue;
											}
										
										folders.push([folder, folderContent, fileIndex]);
									}
									
								//Write list of used folders
									for (var i = 0; i < folders.length; i++) {
										if (!(this.workArea.usedFolders.indexOf(folders[i][0]) + 1)) {
											this.workArea.usedFolders.push(folders[i][0]);
										}
									}
									
								//Add images to queue
									if (this.workArea.usedFolders.length == 1) {
										folder = folders[folders.length - 1];
										
										for (var i = 0; i < folder[1].length; i++) {
											this.workArea.imagesQueue.push(folder[0] + folder[1][i]);
										}
										
										this.workArea.selectImage(folder[2]);
									} else {
										for (var i = 0; i < folders.length; i++) {
											this.workArea.imagesQueue.push(folders[i][0] + folders[i][1][folders[i][2]]);
											
											if (i + 1 >= folders.length) {
												this.workArea.selectImage(i);
											}
										}
									}
								}
					},
					
					init: function() {
						//Add links to elements
							this.workArea.imageElement = document.getElementById("curImage");
							this.shell.dialogs.uiMaskElement = document.getElementById("uiMask");
							this.settings.formElements = document.forms.settingsForm.elements;
						
						//Other
							this.settings.config.read();
							this.settings.update();
					},
					
					shell: {
						window: {						
							minimize: function() {
								win.minimize();
							},
							
							changeState: function() {
								if (chrome.app.window.current().isMaximized()) {
									win.unmaximize();
								} else {
									win.maximize();
								}
							},
							
							close: function() {
								win.close();
							}
						},
						
						dialogs: {
							//Secondary variables
								uiMaskElement: undefined,
							
							//Methods
								show: function(name) {
									this.uiMaskElement.style.top = "0";
									document.getElementById(name).style.top = "15vh";
								},
								
								hide: function(name) {
									this.uiMaskElement.style.top = "-500vh";
									document.getElementById(name).style.top = "-500vh";
								}
						},
						
						hotkeys: {
							//Secondary variables
								isCtrlDown: false,
							
							//Methods
								handle: function(e) {
									switch (e.keyCode) {
										case 116: //F5
											app.workArea.refresh();
											
											break;
										case 37: //Left arrow
											app.workArea.goBack();
											
											break;
										case 39: //Right arrow
											app.workArea.goForward();
											
											break;
										case 189: //Minus
											app.workArea.zoom(false);
											
											break;
										case 187: //Plus
											app.workArea.zoom(true);
											
											break;
										case 17: //Ctrl
											this.isCtrlDown = true;
											
											break;
									}
								},
								
								detectUppedCtrl: function(e) {
									if (e.keyCode == 17) {
										this.isCtrlDown = false;
									}
								}
						}
					},
					
					workArea: {
						//Secondary variables
							imageExtensions: [
								".jpg", ".jpeg", ".jpe", ".jif", ".jfif", ".jfi",
								".webp",
								".gif",
								".png",
								".svg", ".svgz",
								".xbm",
								".bmp",
								".ico"
							],
							
							usedFolders: [],
							imagesQueue: [],
							curImage: 0,
							
							imageElement: undefined,
							originalImageSize: 0,
						
						//Methods
							selectImage: function(number) {
								if (this.imagesQueue.length != 0) {
									//Set curImage
										if (number <= -1) {
											this.curImage = this.imagesQueue.length - 1;
										} else if (number >= this.imagesQueue.length) {
											this.curImage = 0;
										} else {
											this.curImage = number;
										}
									
									//Draw image
										this.imageElement.src = "file://" + this.imagesQueue[this.curImage];
										
										if (app.settings.list.smoothImages) {
											this.imageElement.style = "image-rendering: auto";
										} else {
											this.imageElement.style = "image-rendering: pixelated";
										}
										
										if (app.settings.list.autoZoomImages) {
											this.imageElement.height = innerHeight;
										}
								}
							},
							
							goBack: function() {
								this.selectImage(this.curImage - 1);
							},
							
							goForward: function() {
								this.selectImage(this.curImage + 1);
							},
														
							refresh: function() {
								this.selectImage(this.curImage);
							},
							
							zoom: function(isZoomIn, e) {
								if (isZoomIn) {
									this.imageElement.height += this.originalImageSize * 0.05;
								} else {
									if (this.imageElement.height >= this.originalImageSize * 0.05) {
										this.imageElement.height -= this.originalImageSize * 0.05;
									}
								}
								
								if (e) {
									e.preventDefault();
								}
							}
					}
			};
